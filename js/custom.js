$(document).ready(function () {
  // Menu ---------------------------//
  $('.drop-mob').on('click', function() {
  	
  	// $( this ).parent().
  	var classActive = '#'+$( this ).parent().attr('id') + ' .sub-menu';
  	// console.log(classActive);
    if($(classActive).hasClass('active')) {
      $(classActive).removeClass('active');
    } else {
      $(classActive).addClass('active');
    }
  });
  $('.mob-menu-toggle').on('click', function() {
    if($('body').hasClass('fix_active')) {
      $('body').removeClass('fix_active open_menu');
      $('.fixmenu').fadeOut();
      $('.overlay').fadeOut();
      $('#contents').css('padding-top','');
       // $('.fixmenu').css('top',$('#header').height());
    } else {
      $('.fixmenu').fadeIn();
      $('.overlay').fadeIn();
      $('body').addClass('fix_active open_menu');
      $('#contents').css('padding-top',$('#header').height());
      // $('.fixmenu').css('top',$('#header').height());
    }
  });
  $('.mob-close-toggle').on('click', function() {
    $('body').removeClass('fix_active open_menu');
      $('.fixmenu').fadeOut();
      $('.overlay').fadeOut();
      $('#contents').css('padding-top','');
  });
});
$(window).scroll(function(){
    if ($(window).scrollTop() >= 40) {
        $('#header').addClass('babyst-sticksy ');
        // $('nav div').addClass('visible-title');
    }
    else {
        $('#header').removeClass('babyst-sticksy ');
        // $('nav div').removeClass('visible-title');
    }
});